/******************
 * MOUSE HANDLING *
 ******************/
var mousePosition = { x: 0, y: 0 };

document.addEventListener('pointermove', function(event) {
	mousePosition.x = event.pageX;
	mousePosition.y = event.pageY;
});

/***************
 * GAME ENGINE *
 ***************/
var blocksAlive, blocksDead, bouncer;
var lastFrameTime;

var BLOCKS_X = 120, BLOCKS_Y = 40; // Number of rainbowy dust blocks
var Y_FILL_PERCENT = 0.15; // Percent of top part filled with blocks

var BOUNCER_WIDTH = 0.25, BOUNCER_HEIGHT = 0.02; // Size of bouncer

var N_RAINBOW_CHANGES = 42; // Number of color changes in rainbow pattern

function initialize()
{
	lastFrameTime = Date.now();

	// Rainbowy dust
	blocksDead = [];

	for (var y = 0; y < BLOCKS_Y; y++) {
		for (var x = 0; x < BLOCKS_X; x++) {
			blocksDead.push({
				x: x/BLOCKS_X,
				y: Y_FILL_PERCENT * y/BLOCKS_Y,
				width: 1/BLOCKS_X,
				height: Y_FILL_PERCENT * 1/BLOCKS_Y,
				color: '#FFFFFF',
			});
		}
	}

	// Genesis dust (at center)
	blocksAlive = [];
	blocksAlive.push({
		x: 0.5,
		y: 0.5,
		width: 1/BLOCKS_X,
		height: Y_FILL_PERCENT * 1/BLOCKS_Y,
		color: '#FFFFFF',
		speedX: 0.00,
		speedY: 0.15
	});

	// Bouncer (at bottom vertically and centered horizontally)
	bouncer = {
		x: 0.5 - BOUNCER_WIDTH/2,
		y: 1 - BOUNCER_HEIGHT,
		width: BOUNCER_WIDTH,
		height: BOUNCER_HEIGHT,
		color: '#FFFFFF'
	}
}

function drawRectObject(canvas, context, obj) {
	context.beginPath();
	context.rect(
		obj.x * canvas.width,
		obj.y * canvas.height,
		obj.width * canvas.width,
		obj.height * canvas.height);
	context.fillStyle = obj.color;
	context.fill();
}

function drawRainbowGradient(canvas, context) {
	var gradient = context.createLinearGradient(0, 0, canvas.width, canvas.height);
	var colors = ['#FF0000', '#FF7F00', '#FFFF00', '#00FF00', '#0000FF', '#4B0082', '#7F00FF'];
	for (var i = 0; i <= N_RAINBOW_CHANGES; i++) {
		gradient.addColorStop(i / N_RAINBOW_CHANGES, colors[i % colors.length]);
	}
	context.fillStyle = gradient;

	context.fillRect(0, 0, canvas.width, canvas.height);
}

function redraw() {
	var canvas = document.getElementById('gamecanvas');
	var context = canvas.getContext('2d');

	// Rainbowy dust (just white blocks)
	context.globalCompositeOperation = 'source-over';
	context.clearRect (0, 0, canvas.width, canvas.height);
	for (var i = 0; i < blocksAlive.length; i++) {
		drawRectObject(canvas, context, blocksAlive[i]);
	}
	for (var i = 0; i < blocksDead.length; i++) {
		drawRectObject(canvas, context, blocksDead[i]);
	}

	// Rainbow pattern
	context.globalCompositeOperation = 'source-in';
	drawRainbowGradient(canvas, context);

	// Bouncer (solid color)
	context.globalCompositeOperation = 'source-over';
	drawRectObject(canvas, context, bouncer);
}

function objectCollision(objA, objB) {
	function intervalCollisionRatio(startA, endA, startB, endB) {
		if (startA >= endB || startB >= endA) {
			return 0;
		}

		var incidenceLength = Math.max(startA, startB) - Math.min(endA, endB);
		var maxIncidenceLength = Math.min(endA - startA, endB - startB);
		return incidenceLength / maxIncidenceLength;
	}
	var cw = intervalCollisionRatio(objA.x, objA.x + objA.width, objB.x, objB.x + objB.width);
	var ch = intervalCollisionRatio(objA.y, objA.y + objA.height, objB.y, objB.y + objB.height);
	return cw * ch >= 0.1;
}

function update() {
	var canvas = document.getElementById('gamecanvas');
	var thisFrameTime = Date.now();
	var frameDelta = (thisFrameTime - lastFrameTime) / 1000;

	// Check block collisions
	for (var i = 0; i < blocksAlive.length; i++) {
		// Check scenario top/left/right bounds
		if (blocksAlive[i].x <= 0.0) {
			blocksAlive[i].speedX = Math.abs(blocksAlive[i].speedX);
		} else if (blocksAlive[i].x >= 1.0) {
			blocksAlive[i].speedX = -Math.abs(blocksAlive[i].speedX);
		}
		if (blocksAlive[i].y <= 0.0) {
			blocksAlive[i].speedY = Math.abs(blocksAlive[i].speedY);
		}

		// Check for collision with top blocks
		if (blocksAlive[i].y < Y_FILL_PERCENT) { // Optimization
			for (var j = 0; j < blocksDead.length; j++) {
				if (!blocksDead[j].alive && objectCollision(blocksAlive[i], blocksDead[j])) {
					blocksAlive[i].speedY = Math.abs(blocksAlive[i].speedY);

					var madeAlive = blocksDead[j];
					blocksDead.splice(j, 1);
					j--;
					madeAlive.speedX = (Math.random() + 0.01) * 0.09;
					madeAlive.speedY = (Math.random() + 0.5) * 0.20;
					blocksAlive.push(madeAlive);
				}
			}
		}

		// Check with collision with bouncer
		if (objectCollision(blocksAlive[i], bouncer)) {
			// Project dust into bouncer horizontally, then get number 0.0 to 1.0 indicating hit place
			var horizontalHitPlace = (blocksAlive[i].x + blocksAlive[i].width / 2 - bouncer.x) / bouncer.width;
			blocksAlive[i].speedX = (horizontalHitPlace - 0.5) * 1.50;
			blocksAlive[i].speedY = -blocksAlive[i].speedY * 2;
		}
	}

	// Update block positions
	for (var i = 0; i < blocksAlive.length; i++) {
		blocksAlive[i].x += blocksAlive[i].speedX * frameDelta;
		blocksAlive[i].y += blocksAlive[i].speedY * frameDelta;

		// Kill offbounds blocks
		if (blocksAlive[i].x <= -0.1 || blocksAlive[i].x >= 1.1 ||
			blocksAlive[i].y <= -0.1 || blocksAlive[i].y >= 1.1) {
			blocksAlive.splice(i, 1);
			i--;
		}
	}

	// Update bouncer to follow mouse while remaining inbounds
	bouncer.x = (mousePosition.x - canvas.offsetLeft) / canvas.offsetWidth - bouncer.width / 2;
	bouncer.x = Math.max(bouncer.x, 0);
	bouncer.x = Math.min(bouncer.x, 1.0 - bouncer.width);

	lastFrameTime = thisFrameTime;
}

/*************
 * GAME LOOP *
 *************/
function start()
{
	initialize();
	requestAnimationFrame(onFrame);
}

function onFrame()
{
	redraw();
	update();

	requestAnimationFrame(onFrame);
}

document.addEventListener('DOMContentLoaded', start);
